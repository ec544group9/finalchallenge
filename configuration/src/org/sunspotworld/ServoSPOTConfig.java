/*
 * Copyright (c) 2006-2010 Sun Microsystems, Inc.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to 
 * deal in the Software without restriction, including without limitation the 
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or 
 * sell copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 **/
package org.sunspotworld;

import com.sun.spot.resources.Resources;
import com.sun.spot.resources.transducers.ISwitch;
import com.sun.spot.resources.transducers.ISwitchListener;
import com.sun.spot.resources.transducers.ITriColorLED;
import com.sun.spot.resources.transducers.ITriColorLEDArray;
import com.sun.spot.resources.transducers.LEDColor;
import com.sun.spot.resources.transducers.SwitchEvent;
import com.sun.spot.sensorboard.EDemoBoard;
import com.sun.spot.sensorboard.peripheral.Servo;
import com.sun.spot.service.BootloaderListenerService;
import com.sun.spot.util.Utils;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;
import org.sunspotworld.common.Globals;
import org.sunspotworld.common.TwoSidedArray;
import org.sunspotworld.lib.BlinkenLights;


import com.sun.spot.peripheral.radio.RadioFactory;
import com.sun.spot.resources.Resources;
//import com.sun.spot.resources.transducers.IIOPin;
import com.sun.spot.resources.transducers.IAnalogInput;
//import com.sun.spot.sensorboard.io.AnalogInput;
import com.sun.spot.resources.transducers.ISwitch;
import com.sun.spot.resources.transducers.ITriColorLED;
import com.sun.spot.resources.transducers.ITriColorLEDArray;
import com.sun.spot.sensorboard.EDemoBoard;
import com.sun.spot.service.BootloaderListenerService;
import com.sun.spot.util.IEEEAddress;
import com.sun.spot.util.Utils;

import java.io.IOException;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;

/**
 * This class is used to move a servo car consisting of two servos - one for
 * left wheel and the other for right wheel. To combine these servos properly,
 * this servo car moves calibrateSpeedAndDirection/backward, turn right/left and rotate
 * clockwise/counterclockwise.
 * 
 * The current implementation has 3 modes and you can change these "moving mode" 
 * by pressing sw1. Mode 1 is "Normal" mode moving the car according to the tilt 
 * of the remote controller. Mode 2 is "Reverse" mode moving the car in 
 * a direction opposite to Mode 1. Mode 3 is "Rotation" mode only rotating the
 * car clockwise or counterclockwise according to the tilt.
 * 
 * @author Tsuyoshi Miyake <Tsuyoshi.Miyake@Sun.COM>
 * @author Yuting Zhang<ytzhang@bu.edu>
 */
public class ServoSPOTConfig extends MIDlet
{

    private static final int SERVO_CENTER_VALUE = 1500;
    //private static final int SERVO_MAX_VALUE = 2000;
    //private static final int SERVO_MIN_VALUE = 1000;
    private static final int SERVO1_MAX_VALUE = 2000;
    private static final int SERVO1_MIN_VALUE = 1000;
    private static final int SERVO2_MAX_VALUE = 2000;
    private static final int SERVO2_MIN_VALUE = 1000;
    //private static final int SERVO_HIGH = 500;
    //private static final int SERVO_LOW = 300;
    private static final int SERVO1_HIGH = 20; //steering step high
    private static final int SERVO1_LOW = 10; //steering step low
    private static final int SERVO2_HIGH = 10; //speeding step high
    private static final int SERVO2_LOW = 5; //speeding step low
    private static final int PROG0 = 0; //default program
    private static final int PROG1 = 1; // reversed program
    //private static final int PROG2 = 2;
    // Devices
    private EDemoBoard eDemo = EDemoBoard.getInstance();
    private ISwitch sw1 = eDemo.getSwitches()[EDemoBoard.SW1];
    private ISwitch sw2 = eDemo.getSwitches()[EDemoBoard.SW2];
    private ITriColorLED[] leds = eDemo.getLEDs();
    private ITriColorLEDArray myLEDs = (ITriColorLEDArray) Resources.lookup(ITriColorLEDArray.class);
    // 1st servo for left & right direction 
    private Servo speedServo = new Servo(eDemo.getOutputPins()[EDemoBoard.H1]);
    // 2nd servo for calibrateSpeedAndDirection & backward direction
    private Servo directionServo = new Servo(eDemo.getOutputPins()[EDemoBoard.H0]);
    private BlinkenLights progBlinker = new BlinkenLights(0, 3);
    private BlinkenLights velocityBlinker = new BlinkenLights(4, 7);
    private int current1 = SERVO_CENTER_VALUE;
    private int current2 = SERVO_CENTER_VALUE;
    private int step1 = SERVO1_LOW;
    private int step2 = SERVO2_LOW;
    private int program = PROG0;
    IAnalogInput sensor1 = EDemoBoard.getInstance().getAnalogInputs()[EDemoBoard.A0];//frontright
    IAnalogInput sensor2 = EDemoBoard.getInstance().getAnalogInputs()[EDemoBoard.A1];//frontleft
    IAnalogInput sensor3 = EDemoBoard.getInstance().getAnalogInputs()[EDemoBoard.A2];//backright
    IAnalogInput sensor4 = EDemoBoard.getInstance().getAnalogInputs()[EDemoBoard.A3];//backleft
    //private int servo1ForwardValue;
    //private int servo2ForwardValue;
    private int servo1Left = SERVO_CENTER_VALUE + SERVO1_LOW;
    private int servo1Right = SERVO_CENTER_VALUE - SERVO1_LOW;
    private int servo2Forward = SERVO_CENTER_VALUE + SERVO2_LOW;
    private int servo2Back = SERVO_CENTER_VALUE - SERVO2_LOW;

    public ServoSPOTConfig()
    {
    }

    /** BASIC STARTUP CODE **/
    protected void startApp() throws MIDletStateChangeException
    {
	System.out.println("Hello, world");
	BootloaderListenerService.getInstance().start();

	for (int i = 0; i < myLEDs.size(); i++)
	{
	    myLEDs.getLED(i).setColor(LEDColor.GREEN);
	    myLEDs.getLED(i).setOn();
	}
	Utils.sleep(500);
	for (int i = 0; i < myLEDs.size(); i++)
	{
	    myLEDs.getLED(i).setOff();
	}
	progBlinker.startPsilon();
	velocityBlinker.startPsilon();

	/* Begins the calibration */
	while (sw1.isOpen());
	Utils.sleep(1000);
	calibrateCenter();
	calibrateSpeedAndDirection();
    }

    private void calibrateCenter()
    {
	System.out.println("Calibrating Speed and Angle Center Values");
	speedServo.setValue(0);
	directionServo.setValue(0);
	while(sw1.isOpen())
	{
	    /* Continue calibrating the center value until the switch is pressed */
	    speedServo.setValue(1500);
	    directionServo.setValue(1500);
	    Utils.sleep(10);
	}
    }

    private void calibrateSpeedAndDirection()
    {
	System.out.println("Calibrating Speed and Direction");
	for (int i = 1000; i < 2000; i += 10)
	{
	    speedServo.setValue(i);
	    directionServo.setValue(i);
	    System.out.println("Speed/Direction:" + i);
	    Utils.sleep(500);
	}
    }

    protected void pauseApp()
    {
	// This will never be called by the Squawk VM
    }

    /**
     * Called if the MIDlet is terminated by the system.
     * I.e. if startApp throws any exception other than MIDletStateChangeException,
     * if the isolate running the MIDlet is killed with Isolate.exit(), or
     * if VM.stopVM() is called.
     * 
     * It is not called if MIDlet.notifyDestroyed() was called.
     *
     * @param unconditional If true when this method is called, the MIDlet must
     *    cleanup and release all resources. If false the MIDlet may throw
     *    MIDletStateChangeException  to indicate it does not want to be destroyed
     *    at this time.
     */
    protected void destroyApp(boolean unconditional) throws MIDletStateChangeException
    {
	for (int i = 0; i < myLEDs.size(); i++)
	{
	    myLEDs.getLED(i).setOff();
	}
    }
}

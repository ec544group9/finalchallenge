/*
 * TemperatureBasestation.java
 *
 * Copyright (c) 2008-2009 Sun Microsystems, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package challenge2.desktop;

import com.sun.spot.io.j2me.radiogram.*;
import com.sun.spot.peripheral.ota.OTACommandServer;
import com.sun.spot.peripheral.radio.BasestationManager;
import com.sun.spot.peripheral.radio.IRadioPolicyManager;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.microedition.io.*;
import com.sun.spot.peripheral.radio.RadioFactory;


/**
 * This application is the 'on Desktop' portion of the SendDataDemo. 
 * This host application collects sensor samples sent by the 'on SPOT'
 * portion running on neighboring SPOTs and just prints them out. 
 *   
 * @author: Vipul Gupta
 * modified: Ron Goldman
 *
 * This program was altered for the use in challenge 6, for Group 9
 * @author: Group 9
 */
public class Basestation {
    // Broadcast port on which we listen for sensor samples
    private static final int HOST_PORT = 20;
    private static final int CHANNEL_NUMBER = 19;
    private static final short PAN_ID = 9;
        
    private void run() throws Exception {
        IRadioPolicyManager iRPM = RadioFactory.getRadioPolicyManager();
        iRPM.setChannelNumber(CHANNEL_NUMBER);
        iRPM.setPanId(PAN_ID);
        
        RadiogramConnection rCon;
        Datagram dg;
       
         
        try {
            // Open up a server-side broadcast radiogram connection
            // to listen for sensor readings being sent by different SPOTs
            rCon = (RadiogramConnection) Connector.open("radiogram://:" + HOST_PORT);
            dg = rCon.newDatagram(rCon.getMaximumLength());
        } catch (Exception e) {
             System.err.println("setUp caught " + e.getMessage());
             throw e;
        }
     
        String path = getClass().getClassLoader().getResource(".").getPath();
        System.out.println(path);
        File file = new File("/Users/Raphy/Documents/School/Fall2013/EC544/FinalChallenge/FinalChallengeServer/spotData.txt");
        
        // if file doesnt exists, then create it
        if (!file.exists()) 
        {
            file.createNewFile();
        }
        else
        {
            file.delete();
            file.createNewFile();
        }
        
        // Main data collection loop
        while (true) {
            try {
                // Read sensor sample received over the radio
                rCon.receive(dg);
                long order;
                order = dg.readLong();  // read sender's Id                
                

                System.out.println(order);
                
                FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
                BufferedWriter bw = new BufferedWriter(fw);
                String content = order + "\n";
                bw.write(content);
                bw.close();
                
            } catch (Exception e) {
                System.err.println("Caught " + e +  " while reading sensor samples.");
                throw e;
            }
        }
    }
    
    /**
     * Start up the host application.
     *
     * @param args any command line arguments
     */
    public static void main(String[] args) throws Exception {
        // register the application's name with the OTA Command server & start OTA running 
        OTACommandServer.start("SendDataDemo");

        Basestation app = new Basestation();
        app.run();
    }
}

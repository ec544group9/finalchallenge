Hello!
====

This is the final server for the Final Challenge in EC544.  It proovides a view to the general position of the car at a given time.  Since the fine 
localization is not possible, we instead use general location, when the car passes a beacon, the car sends the number of the beacon it passed
to the server and the server image is appropriately updated.

Go!
===

Download and run sinatra-bootstrap:

	gem install sinatra
	gem install thin
	gem install haml

	# Then run the server
	thin start

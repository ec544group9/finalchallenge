require 'sinatra/base'
require 'EventMachine'
require 'haml'
require 'date'

class CarServer < Sinatra::Base
	configure do
		set :public_folder, Proc.new { File.join(root, "static") }
		set :views, Proc.new{ File.join(root, "views") }
		enable :sessions
		set server: 'thin'
		set :raise_errors => true
		set :logging, true
		set :environment, :production
	end

	# Main page, nothing special
	get '/' do
		haml :index
	end

	# Contains the localization image
	get '/localizer' do
		haml :localizer
	end

	get '/about' do
		haml :about
	end

	# The text stream that will provide the real time data on position
	get '/localizer/datastream', provides: 'text/event-stream' do
		stream :keep_open do |out|
			@connections << out

			# Callback on close event
			out.callback {
				# Delete the connection
				@connections.delete(out)
			}
		end
	end

	def initialize(app = nil)
		@connections = []
		@notifications = ""
		@xPos = 30
		@yPos = 30
		@xDel = 5
		@yDel = 5
		@demo = false

		EM::next_tick do
			EM::add_periodic_timer(0.5) do
				if (@demo)
					@xPos += @xDel
					if (@xPos > 365 || @xPos < 0)
						@xDel *= -1
						@xPos += @xDel
					end

					@yPos += @yDel;
					if (@yPos > 385 || @yPos < 0)
						@yDel *= -1
						@yPos += @yDel
					end

					@notifications = @xPos.to_s + " " + @yPos.to_s
				else
					size = File.size?("/Users/Raphy/Documents/School/Fall2013/EC544/FinalChallenge/FinalChallengeServer/spotData.txt")
					# Only get new beacon if size is not 0
					if (size)
						f = File.new("/Users/Raphy/Documents/School/Fall2013/EC544/FinalChallenge/FinalChallengeServer/spotData.txt")
						# Rather than coordinates, now it is simply a beacon number, and the server lights up the appropriate beacon
						coords = []
						while line = f.gets
							if (line.length < 1)
								next
							end
							coords.push(line.split(' '))
						end
						#Prints the notification, which is just a number (the current beacon it is passing)
						coords.each do |set|
							@notifications += set[0].to_s + "\n"
							p @notifications
						end
					end
				end

				if (@notifications != "")
					@connections.each do |out|
						out << "data:#{@notifications}\n\n"
					end
				end
				
				# Clear the notification
				@notifications = ""

				# # Empty the file
				File.open('/Users/Raphy/Documents/School/Fall2013/EC544/FinalChallenge/FinalChallengeServer/spotData.txt', 'w') {}
			end
		end
		super(app)
	end
end
/*
 * Copyright (c) 2006-2010 Sun Microsystems, Inc.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to 
 * deal in the Software without restriction, including without limitation the 
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or 
 * sell copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permissiIon notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 **/
package org.sunspotworld;

import com.sun.spot.io.j2me.radiogram.Radiogram;
import com.sun.spot.io.j2me.radiogram.RadiogramConnection;
import com.sun.spot.peripheral.Spot;
import com.sun.spot.peripheral.TimeoutException;
import com.sun.spot.peripheral.radio.IProprietaryRadio;
import com.sun.spot.peripheral.radio.IRadioPolicyManager;
import com.sun.spot.peripheral.radio.RadioFactory;
import com.sun.spot.resources.transducers.ISwitchListener;
import com.sun.spot.resources.transducers.LEDColor;
import com.sun.spot.resources.transducers.SwitchEvent;
import com.sun.spot.sensorboard.peripheral.Servo;
import org.sunspotworld.lib.BlinkenLights;


import com.sun.spot.resources.Resources;
//import com.sun.spot.resources.transducers.IIOPin;
import com.sun.spot.resources.transducers.IAnalogInput;
//import com.sun.spot.sensorboard.io.AnalogInput;
import com.sun.spot.resources.transducers.ISwitch;
import com.sun.spot.resources.transducers.ITriColorLED;
import com.sun.spot.resources.transducers.ITriColorLEDArray;
import com.sun.spot.sensorboard.EDemoBoard;
import com.sun.spot.service.BootloaderListenerService;
import com.sun.spot.util.IEEEAddress;
import com.sun.spot.util.Utils;

import java.io.IOException;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;
import java.lang.Math;
import javax.microedition.io.Connector;
import javax.microedition.io.Datagram;

/**
 * This class is used to move a servo car consisting of two servos - one for
 * left wheel and the other for right wheel. To combine these servos properly,
 * this servo car moves forward/backward, turn right/left and rotate
 * clockwise/counterclockwise.
 *
 * The current implementation has 3 modes and you can change these "moving mode"
 * by pressing sw1. Mode 1 is "Normal" mode moving the car according to the tilt
 * of the remote controller. Mode 2 is "Reverse" mode moving the car in a
 * direction opposite to Mode 1. Mode 3 is "Rotation" mode only rotating the car
 * clockwise or counterclockwise according to the tilt.
 *
 * @author Tsuyoshi Miyake <Tsuyoshi.Miyake@Sun.COM>
 * @author Yuting Zhang<ytzhang@bu.edu>
 */
public class ServoSPOTonCar extends MIDlet implements ISwitchListener {

    private static final int SERVO_CENTER_VALUE = 1500;
    //private static final int SERVO_MAX_VALUE = 2000;
    //private static final int SERVO_MIN_VALUE = 1000;
    private static final int SERVO1_MAX_VALUE = 2000;
    private static final int SERVO1_MIN_VALUE = 1000;
    private static final int SERVO2_MAX_VALUE = 2000;
    private static final int SERVO2_MIN_VALUE = 1000;
    //private static final int SERVO_HIGH = 500;
    //private static final int SERVO_LOW = 300;
    private static final int SERVO1_HIGH = 20; //steering step high
    private static final int SERVO1_LOW = 10; //steering step low
    private static final int SERVO2_HIGH = 10; //speeding step high
    private static final int SERVO2_LOW = 5; //speeding step low
    private static final int PROG0 = 0; //default program
    private static final int PROG1 = 1; // reversed program
    //private static final int PROG2 = 2;
    
    /* The scanner for getting user input in calibrate forwardSpeed and angle */
    // Devices
    private EDemoBoard eDemo = EDemoBoard.getInstance();
    private ISwitch sw1 = eDemo.getSwitches()[EDemoBoard.SW1];
    private ISwitch sw2 = eDemo.getSwitches()[EDemoBoard.SW2];
    private ITriColorLED[] leds = eDemo.getLEDs();
    private ITriColorLEDArray myLEDs = (ITriColorLEDArray) Resources.lookup(ITriColorLEDArray.class);
    // 1st servo for forwardSpeed 
    private Servo speedServo = new Servo(eDemo.getOutputPins()[EDemoBoard.H1]);
    // 2nd servo for direction
    private Servo directionServo = new Servo(eDemo.getOutputPins()[EDemoBoard.H0]);
    private BlinkenLights progBlinker = new BlinkenLights(0, 3);
    private BlinkenLights velocityBlinker = new BlinkenLights(4, 7);
    private int current1 = SERVO_CENTER_VALUE;
    private int current2 = SERVO_CENTER_VALUE;
    private int step1 = SERVO1_LOW;
    private int step2 = SERVO2_LOW;
    private int program = PROG0;
    IAnalogInput frSensor = EDemoBoard.getInstance().getAnalogInputs()[EDemoBoard.A0];//frontright
    IAnalogInput flSensor = EDemoBoard.getInstance().getAnalogInputs()[EDemoBoard.A1];//frontleft
    IAnalogInput brSensor = EDemoBoard.getInstance().getAnalogInputs()[EDemoBoard.A2];//backright
    IAnalogInput blSensor = EDemoBoard.getInstance().getAnalogInputs()[EDemoBoard.A3];//backleft
    //private int servo1ForwardValue;
    //private int servo2ForwardValue;
    private int servo1Left = SERVO_CENTER_VALUE + SERVO1_LOW;
    private int servo1Right = SERVO_CENTER_VALUE - SERVO1_LOW;
    private int servo2Forward = SERVO_CENTER_VALUE + SERVO2_LOW;
    private int servo2Back = SERVO_CENTER_VALUE - SERVO2_LOW;
    private int forwardSpeed = 1200;
    private int backwardSpeed = 1600;
    private boolean enableCalibration = false;
    private boolean calibrateSpeed = false;
    private boolean calibrateAngle = false;
    
    private int maxAngle = 15;
    private int wallDistance = 60;
    
    private double p1 = -10.76;
    private double p2 = 67.71;
    private double p3 = -163.9;
    private double p4 = 169.3;
    
    private static final String VERSION = "1.0";
    // CHANNEL_NUMBER  default as 26, each group set their own correspondingly
    private static final int CHANNEL_NUMBER = 19; 
    private static final short PAN_ID               = 9;
    private static final String BROADCAST_PORT      = "19";
    private static final int PACKETS_PER_SECOND     = 1;
    private static final int PACKET_INTERVAL        = 1000 / PACKETS_PER_SECOND;
    private int power = 32;         
    // Start with max transmit power
    private int xtilt=0;
    private int ytilt=0;
//    private ISwitch sw1 = (ISwitch)Resources.lookup(ISwitch.class, "SW1");
//    private ISwitch sw2 = (ISwitch)Resources.lookup(ISwitch.class, "SW2");
    private ITriColorLEDArray leds_arr = (ITriColorLEDArray)Resources.lookup(ITriColorLEDArray.class);
//    private ITriColorLED statusLED = leds_arr.getLED(0);//connection indicator
//    private ITriColorLED xmitLED = leds_arr.getLED(7);//sending request indicator
//    private ITriColorLED recvLED = leds_arr.getLED(6);//receving distance indicator
    private ITriColorLED passLED = leds_arr.getLED(2);
//    private IAccelerometer3D accel = (IAccelerometer3D)Resources.lookup(IAccelerometer3D.class);
//    private ILightSensor light = (ILightSensor)Resources.lookup(ILightSensor.class);

    private LEDColor red   = new LEDColor(100,0,0);
    private LEDColor green = new LEDColor(0,100,0);
    private LEDColor blue  = new LEDColor(0,0,100);
    
    private boolean xmitDo = true;
    private boolean recvDo = true;
    
    private long myAddr;
    private long TimeStamp;
   
    private String[] beacon={"0014.4F01.0000.45A9", "0014.4F01.0000.4120","0014.4F01.0000.80F5","0014.4F01.0000.7DA5"};
    private int order=0;
 
    private boolean turning = false;
    private boolean printBeaconMessages = true;
    private boolean printVoltageMessage = false;
    private boolean printDistanceMessage = false;
    private boolean remoteControlled = true;
    private final double thresholdBeacon = 114;
    
    private boolean threePoint = false;
    
    /* The last beacon to send a turn message */
    private long lastBeacon = -1;
    
    /* Synchronized turning so communication thread can change state in thread safe way */
    private synchronized void changeTurning(boolean state)
    {
        turning = state;
    }

    public ServoSPOTonCar() {
    }

    /**
     * BASIC STARTUP CODE *
     */
    protected void startApp() throws MIDletStateChangeException {
        System.out.println("Hello, world");
        BootloaderListenerService.getInstance().start();
        sw2.addISwitchListener(this);
  
        new com.sun.spot.service.BootloaderListenerService().getInstance().start();
        initialize();
        System.out.println("Radio Signal Strength Test (version " + VERSION + ")");
        System.out.println("Packet interval = " + PACKET_INTERVAL + " msec");
	
	/* Calibrate before launching threads */
	calibrateCenter();
        
	/* The main car control */
        new Thread()
        {
            public void run()
            {
                try {
                    car_control();
                } catch (MIDletStateChangeException ex) {
                    ex.printStackTrace();
                }
            }
        }.start(); 
	
        new Thread() {
            public void run () {
                xmitLoop();
            }
        }.start();                      // spawn a thread to transmit packets
        
        new Thread() {
            public void run () {
                recvLoop();
            }
        }.start();                      // spawn a thread to receive packets
      
	   
    }
    
    private void car_control() throws MIDletStateChangeException{
	/* Flash Green to Say Hello */
	myLEDs.getLED(0).setColor(LEDColor.GREEN);
	myLEDs.getLED(7).setColor(LEDColor.GREEN);
	
	myLEDs.getLED(0).setOn();
	myLEDs.getLED(7).setOn();
	pause(500);
	myLEDs.getLED(0).setOff();
	myLEDs.getLED(7).setOff();
	
	/* Hold RED until the button is pressed */
	myLEDs.getLED(0).setColor(LEDColor.RED);
	myLEDs.getLED(7).setColor(LEDColor.RED);
	myLEDs.getLED(0).setOn();
	myLEDs.getLED(7).setOn();

	
        /* Do not go until the switch is pressed */	
	while (sw1.isOpen());        
        for (int ii = 0 ; ii < 2 ; ii++)
        {
            myLEDs.getLED(0).setColor(LEDColor.GREEN);
	    myLEDs.getLED(7).setColor(LEDColor.GREEN);

	    myLEDs.getLED(0).setOn();
	    myLEDs.getLED(7).setOn();
	    pause(500);
	    myLEDs.getLED(0).setOff();
	    myLEDs.getLED(7).setOff();
	    
            pause(500);
        }
  
        
       double carAngle = 0;
       DistPair distances = null;
       beginForwardMotion();
       long alternating = 0;
	
        while (true) {  
            if (remoteControlled)
            {
//                System.out.println("controled by remote");
                System.out.println(xtilt + " " + ytilt);
                 controlMotion(xtilt, ytilt);
            }
            else
            {
                System.out.println("controlled by itself");
                if (alternating % 2 == 0)
                {
                    speedServo.setValue(forwardSpeed);
                }
                else if (alternating % 2 == 1)
                {
                    speedServo.setValue(forwardSpeed + 10);
                }

                /* Get the sensor and distance/angle values */
                try
                {
                    double fr = 0.0;
                    double fl = 0.0;
                    double br = 0.0;
                    double bl = 0.0;

                    fr = frSensor.getVoltage();
                    fl = flSensor.getVoltage();
                    br = brSensor.getVoltage();
                    bl = blSensor.getVoltage();

                    if (printVoltageMessage)
                        System.out.println("fr = " + fr + " fl = " + fl + " br = " + br + " bl = " + bl);

                    fl = p1 * (pow(fl, 3)) + p2 * (pow(fl, 2)) + p3 * fl + p4;
                    fr = p1 * (pow(fr, 3)) + p2 * (pow(fr, 2)) + p3 * fr + p4;
                    bl = p1 * (pow(bl, 3)) + p2 * (pow(bl, 2)) + p3 * bl + p4;
                    br = p1 * (pow(br, 3)) + p2 * (pow(br, 2)) + p3 * br + p4;
    //                br = -26.179 * (pow(br, 3)) + 150.86 * (pow(br, 2)) + -302.67 * br + 238.63;

    //		fr = 1 / (fr * 0.0175 + 0.000575);
    //		fl = 1/(fl * 0.0189 -0.0047);
    //		br = 1/(br * 0.0168 -0.0003);
    //		bl = 1/(bl * 0.0175 -0.0017);

                    carAngle = getCurrentAngle(fr, fl, br, bl);
                    distances = getCurrentDistance(fr, fl, br, bl);
                }
                catch(IOException ioe)
                {
                        System.out.println("Error getting infared readings...");
                }

                if (!turning)
                {
                    adjustMovement(carAngle, distances);
                }
                else
                {
                    performTurn(carAngle, distances);
                    changeTurning(false);
		    order = (order + 1) >= beacon.length ? 0 : order + 1; 
                    System.out.println("turn left");
                }

                alternating++;
                speedServo.setValue(0);
    //            pause(1500);
    //            
    //            /* If angle or distance is sufficiently off, can force the car to stop to fix it */
    //            
    //            distanceOff = getCurrentDistance();
    //            adjustDistance(10.0, distanceOff);


               // check();
    //            pause(20);
            }
        }
	
    }
    
    private void calibrateCenter()
    {
	System.out.println("Calibrating Speed and Angle Center Values");
	speedServo.setValue(0);
	directionServo.setValue(0);
	while(sw1.isOpen())
	{
	    /* Continue calibrating the center value until the switch is pressed */
	    speedServo.setValue(1500);
	    directionServo.setValue(1500);
	    Utils.sleep(10);
	}
    }
       
    /**
     * Loop to continually broadcast message.
     * Transmits the address of self, and the order, which is the next beacon which should 
     * return the distance
     */
        private void xmitLoop () {
 //       ILed led = Spot.getInstance().getGreenLed();
        RadiogramConnection txConn = null;
        xmitDo = true;
        while (xmitDo) {
            try {
                txConn = (RadiogramConnection)Connector.open("radiogram://broadcast:" + BROADCAST_PORT);
                txConn.setMaxBroadcastHops(1);      // don't want packets being rebroadcasted
                Datagram xdg = txConn.newDatagram(txConn.getMaximumLength());

		TimeStamp = System.currentTimeMillis();

		xdg.reset();
                xdg.writeInt(3);
		xdg.writeLong(myAddr); // own MAC address
		xdg.writeInt(order);
		txConn.send(xdg);

		pause(200);

		if (printBeaconMessages)
		    System.out.println("sending to"+order+"beacon");
		long delay = (TimeStamp+ PACKET_INTERVAL- System.currentTimeMillis());
		if (delay > 0) {
		    pause(delay);
		}
            } catch (IOException ex) {
                // ignore
            } finally {
                if (txConn != null) {
                    try {
                        txConn.close();
                    } catch (IOException ex) { }
                }
            }
        }
    }
    
        
        
    private void initialize() { 
        myAddr = RadioFactory.getRadioPolicyManager().getIEEEAddress();
//        statusLED.setColor(red);     // Red = not active
//        statusLED.setOn();
        
        IRadioPolicyManager rpm = Spot.getInstance().getRadioPolicyManager();
        rpm.setChannelNumber(CHANNEL_NUMBER);
        rpm.setPanId(PAN_ID);
        rpm.setOutputPower(power - 32);
    //    AODVManager rp = Spot.getInstance().
        pause(1000);
//        statusLED.setOff();
    }    
        
        
        
    /**
     * Receives packets from the basestation/beacons, and interprets them to see if 
     * the car has passed a beacon
     */
    private void recvLoop () {
        RadiogramConnection rcvConn = null;
        recvDo = true;

        while (recvDo) {
            try {
                rcvConn = (RadiogramConnection)Connector.open("radiogram://:" + BROADCAST_PORT);
                rcvConn.setTimeout(PACKET_INTERVAL * 2);
                Radiogram rdg = (Radiogram)rcvConn.newDatagram(rcvConn.getMaximumLength());

                    try {
                        rdg.reset();
                        rcvConn.receive(rdg);           // listen for a packet

                        int type = rdg.readInt();       
//                        System.out.println("package code number: "+type);
                        if (type == 1){
                            int code = rdg.readInt();
                            if (code == 1)
                            {
                                remoteControlled=true;
                            }
			    else if (code == 2)
			    {
				/* Special remote controlled three point turn */
				System.out.println("Got turn request");
				changeTurning(true);
			    }
                            else
                            {
                                remoteControlled=false;
                            }
                            xtilt=rdg.readInt();
                            ytilt=rdg.readInt();
                            
                           
                        }
                        else if (type==2 && !remoteControlled)
			{                                          
                            long bcAddr = rdg.readLong();
                            long bcTime = rdg.readLong();
                            double dist = rdg.readDouble();
                            int pow = rdg.readInt();

                            String srcID = IEEEAddress.toDottedHex(bcAddr);
    //                            System.out.println(srcID+','+bcTime+','+dist);

                            long beaconadd = IEEEAddress.toLong(beacon[order]);
			    long number = order;
			    System.out.println("Beacon message from " + bcAddr + ". Want " + beaconadd);
                            if(bcAddr==beaconadd)
			    {
				if (lastBeacon != bcAddr)
				{
				    lastBeacon = bcAddr;
				    if(dist<= thresholdBeacon && dist > 0)
				    {
					if (printBeaconMessages)
					    System.out.println("pass through"+order+"beacon"); 
					passLED.setColor(red);
					passLED.setOn();
					//                             
					
					System.out.println("New beacon number " + order);
					changeTurning(true);

					for(int jjj=0;jjj<2;jjj++){
					    RadiogramConnection txConn = null;
						try {
						    txConn = (RadiogramConnection)Connector.open("radiogram://broadcast:" + 20);
						    txConn.setMaxBroadcastHops(1);      // don't want packets being rebroadcasted
						    Datagram xdg = txConn.newDatagram(txConn.getMaximumLength());

							TimeStamp = System.currentTimeMillis();
							System.out.println("Sending order " + number);
							xdg.reset();
							xdg.writeLong(number); // own MAC address

							txConn.send(xdg);

						 } catch (IOException ex) {
						// ignore
						 } finally {
						    if (txConn != null) {
							try {
							    txConn.close();
							} catch (IOException ex) { }
						}
					    }
					}

				    }
				    else if (dist < 0){
					if (printBeaconMessages)
					    System.out.println(order+"beacon is not working properly!");     
				    } 
				}
			    }

                            passLED.setOff();
                        }
                    } catch (TimeoutException tex) {        // timeout - display no packet received
			if (printBeaconMessages)
			    System.out.println("time out!");
                    }               
            } catch (IOException ex) {
                // ignore
            } finally {
                if (rcvConn != null) {
                    try {
                        rcvConn.close();
                    } catch (IOException ex) { }
                }
            }
        }
    }
    
    
    private void controlMotion(int xtilt, int ytilt) {
        if (ytilt > 40) {
            forward();
            System.out.println("forward");
        } else if (ytilt < -40) {
            backward();
            System.out.println("backward");
        } 
        else
        {
            stop();
        }
        
        if (xtilt > 40) {
            rcRight();
            System.out.println("right");
        } else if (xtilt < -40) {
            rcLeft();
            System.out.println("left");
        } else {
            rcStraight();
        }
    }
    /**
     * Pause for a specified time.
     *
     * @param time the number of milliseconds to pause
     */
    private void pause (long time) {
        try {
            Thread.currentThread().sleep(time);
        } catch (InterruptedException ex) { /* ignore */ }
    }
    
    
    
    
    public double pow(double x, double y) {
        int den = 1024; //declare the denominator to be 1024  
        /*Conveniently 2^10=1024, so taking the square root 10  
        times will yield our estimate for n.��In our example  
        n^3=8^2n^1024 = 8^683.*/
        int num = (int) (y * den); // declare numerator
        int iterations;
        iterations = 10;
        double n = Double.MAX_VALUE; /* we initialize our
         * estimate, setting it to max*/
        while (n >= Double.MAX_VALUE && iterations > 1) {
            /*��We try to set our estimate equal to the right
             * hand side of the equation (e.g., 8^2048).��If this
             * number is too large, we will have to rescale. */
            n = x;
            for (int i = 1; i < num; i++) {
                n *= x;
            }
            /*here, we handle the condition where our starting
             * point is too large*/
            if (n >= Double.MAX_VALUE) {
                iterations--;
                den = (int) (den / 2);
                num = (int) (y * den); //redefine the numerator
            }
        }
        /*************************************************  
         ** We now have an appropriately sized right-hand-side.  
         ** Starting with this estimate for n, we proceed.  
         **************************************************/
        for (int i = 0; i < iterations; i++) {
            n = Math.sqrt(n);
        }
        // Return our estimate
        return n;
    }
    
    private void leftHandTurn(long forwardTime, long backwardTime)
    {
        /* Demonstrate Turn */
        left(forwardTime);
	if (backwardTime != 0)
	{
	    beginBackwardMotion();
	    right(backwardTime);
	}
        beginForwardMotion();
    }

    private double getCurrentAngle(double fright, double fleft, double bright, double bleft) 
    {
        double fr = fright;
        double fl = fleft;
        double br = bright;
        double bl = bleft;
	
//        System.out.println("fr:"+fr+"fl:"+fl);
        if (fr < fl)
        {
//            if (Math.abs(fr - br) < 50)
//            {
                return fr-br;
//            }
//            else
//            {
//                return 0;
//            }
        }
        else
        {
//            if (Math.abs(fl-bl) < 50)
//            {
                return bl-fl;
//            }
//            else
//            {
//                return 0;
//            }
        }
    }

    /**
     * Returns the right and left distances according to the front two sensors
     *
     * @return
     */
    private DistPair getCurrentDistance(double fright, double fleft, double bright, double bleft) 
    {
        double fr = fright;
        double fl = fleft;
        double br = bright;
        double bl = bleft;

//        double distDelta = Math.abs(rightAve - leftAve);
        if (printDistanceMessage)
	    System.out.println("front left= " + fl + " front Right = " + fr +" backright "+br+" backleft "+bl);

        return new DistPair(fl, fr);
    }

    private void setServoForwardValue() {
        servo1Left = current1 + step1;
        servo1Right = current1 - step1;
        servo2Forward = current2 + step2;
        servo2Back = current2 - step2;
        if (step2 == SERVO2_HIGH) {
            velocityBlinker.setColor(LEDColor.GREEN);
        } else {
            velocityBlinker.setColor(LEDColor.BLUE);
        }
    }
    
    private void performTurn(double diff, DistPair dist) throws MIDletStateChangeException
    {
        for (int i = 0; i < myLEDs.size(); i++) {
            myLEDs.getLED(i).setOff();
        }
        
        double leftDist = dist.getLeft();
        double rightDist = dist.getRight();
        if (Math.abs(diff) < maxAngle)
        {
            myLEDs.getLED(0).setColor(LEDColor.GREEN);
            myLEDs.getLED(7).setColor(LEDColor.GREEN);
	    if (threePoint)
		leftHandTurn(2000, 1250);
	    else
		leftHandTurn(2800, 0);
        }
        else if(diff > maxAngle && rightDist < wallDistance )
        {
            /* Close to right wall, angled left, shorten turn time */
            myLEDs.getLED(0).setColor(LEDColor.BLUE);
            myLEDs.getLED(7).setColor(LEDColor.BLUE);
	    if(threePoint)
		leftHandTurn(1500, 750);
	    else
		leftHandTurn(2100, 0);
        }
        else if (diff > maxAngle && leftDist < wallDistance)
        {
            /* Close to left wall but angled left, turn right first then turn */
            myLEDs.getLED(0).setColor(LEDColor.PUCE);
            myLEDs.getLED(7).setColor(LEDColor.PUCE);
            right(500);
	    if (threePoint)
		leftHandTurn(2000, 1250);
	    else
		leftHandTurn(2600, 0);

        }
        else if (diff < -maxAngle && rightDist < wallDistance)
        {
            /* Angled right and close to right, do an extra long turn */
            myLEDs.getLED(0).setColor(LEDColor.YELLOW);
            myLEDs.getLED(7).setColor(LEDColor.YELLOW);
	    if (threePoint)
		leftHandTurn(2500, 1200);
	    else
		leftHandTurn(3100, 0);
        }
        else if (diff < -maxAngle && leftDist < wallDistance) 
        {
            /* Angled right and close to left, do an extra extra long turn */
            myLEDs.getLED(0).setColor(LEDColor.RED);
            myLEDs.getLED(7).setColor(LEDColor.RED);
	    if (threePoint)
		leftHandTurn(2500, 1200);
	    else
		leftHandTurn(3100, 0);
        }
        else if (diff > maxAngle)
        {
            myLEDs.getLED(0).setColor(LEDColor.CYAN);
            myLEDs.getLED(7).setColor(LEDColor.CYAN);
	    if (threePoint)
		leftHandTurn(1500, 1000);
	    else
		leftHandTurn(2500, 0);
        }
        else if (diff < -maxAngle)
        {
            myLEDs.getLED(0).setColor(LEDColor.MAGENTA);
            myLEDs.getLED(7).setColor(LEDColor.MAGENTA);
	    if (threePoint)
		leftHandTurn(2000, 1200);
	    else
		leftHandTurn(2500, 0);
        }
	else
	{
	    myLEDs.getLED(0).setColor(LEDColor.GREEN);
            myLEDs.getLED(7).setColor(LEDColor.GREEN);
	    if (threePoint)
		leftHandTurn(2000, 1250);
	    else
		leftHandTurn(2800, 0);
	}
    }

    private void adjustMovement(double diff, DistPair dist) throws MIDletStateChangeException 
    {
//        System.out.println("diff:"+diff+" distance:"+distanceOff);
        for (int i = 0; i < myLEDs.size(); i++) {
            myLEDs.getLED(i).setOff();
        }
        
        double leftDist = dist.getLeft();
        double rightDist = dist.getRight();
        if (Math.abs(diff) < maxAngle)
        {
            if (leftDist < rightDist)
            {
                /* Focus on the left wall */
                if (leftDist < wallDistance)
                {
		    if (diff > 3)
		    {
			myLEDs.getLED(7).setColor(LEDColor.BLUE);
			myLEDs.getLED(7).setOn();
			right(400);
			straight(100);
		    }
		    else if (diff > -3)
		    {
			myLEDs.getLED(7).setColor(LEDColor.BLUE);
			myLEDs.getLED(7).setOn();
			right(100);
			straight(400);
		    }
		    else
		    {
			myLEDs.getLED(7).setColor(LEDColor.BLUE);
			myLEDs.getLED(7).setOn();
			straight(450);
			left(50);
		    }
                    
                }
                else
                {
                    myLEDs.getLED(3).setColor(LEDColor.GREEN);
                    myLEDs.getLED(4).setColor(LEDColor.GREEN);
                    myLEDs.getLED(4).setOn();
                    myLEDs.getLED(3).setOn();
                    straight(500);
                }
            }
            else
            {
                /* Focus on the right wall */
                if (rightDist < wallDistance)
                {

		    if (diff < -3 )
		    {
			myLEDs.getLED(0).setColor(LEDColor.BLUE);
			myLEDs.getLED(0).setOn();
			left(400);
			straight(100);
		    }
		    else if (diff < 3)
		    {
			myLEDs.getLED(0).setColor(LEDColor.BLUE);
			myLEDs.getLED(0).setOn();
			left(100);
			straight(400);
		    }
		    else
		    {
			myLEDs.getLED(0).setColor(LEDColor.BLUE);
			myLEDs.getLED(0).setOn();
			straight(450);
			right(50);
		    }
                }
                else
                {
                    myLEDs.getLED(3).setColor(LEDColor.GREEN);
                    myLEDs.getLED(4).setColor(LEDColor.GREEN);
                    myLEDs.getLED(4).setOn();
                    myLEDs.getLED(3).setOn();
                    straight(500);
                }
            }
        }
        else if(diff > maxAngle && rightDist < wallDistance )
        {
                
           myLEDs.getLED(0).setColor(LEDColor.YELLOW);
           myLEDs.getLED(0).setOn();
               right(200);
	       straight(300);
        }
        else if (diff > maxAngle && leftDist < wallDistance)
        {
            myLEDs.getLED(7).setColor(LEDColor.YELLOW);
            myLEDs.getLED(7).setOn();
                right(500);
        }
        else if (diff < -maxAngle && rightDist < wallDistance)
        {
            myLEDs.getLED(0).setColor(LEDColor.RED);
            myLEDs.getLED(0).setOn();
                left(500);
        }
        else if (diff < -maxAngle && leftDist < wallDistance) 
        {
            myLEDs.getLED(7).setColor(LEDColor.RED);
            myLEDs.getLED(7).setOn();
                left(200);
		straight(300);
        }
//        else if (diff > maxAngle)
//        {
//            myLEDs.getLED(7).setColor(LEDColor.MAUVE);
//            myLEDs.getLED(7).setOn();
//            right(200);
//	    straight(300);
//        }
//        else if (diff < -maxAngle)
//        {
//            myLEDs.getLED(0).setColor(LEDColor.MAUVE);
//            myLEDs.getLED(0).setOn();
//            left(200);
//	    straight(300);
//        }
        else
        {
            myLEDs.getLED(3).setColor(LEDColor.GREEN);
            myLEDs.getLED(4).setColor(LEDColor.GREEN);
            myLEDs.getLED(4).setOn();
            myLEDs.getLED(3).setOn();
            straight(500);
        }
	
	pause(200);
    }

    /**
     * Starts the car moving forward very slowly
     */
    private void beginForwardMotion() 
    {
        myLEDs.getLED(1).setColor(LEDColor.MAUVE);
        myLEDs.getLED(1).setOn();
        
        directionServo.setValue(1500);
        speedServo.setValue(forwardSpeed);
    }
    
    private void beginBackwardMotion()
    {
        myLEDs.getLED(1).setColor(LEDColor.TURQUOISE);
        myLEDs.getLED(1).setOn();
        
        directionServo.setValue(1500);
        speedServo.setValue(backwardSpeed);
    }
    
    /* The following are all used for remote control */
    private void forward() 
    {
        speedServo.setValue(forwardSpeed);
    }
    private void backward()
    {
        speedServo.setValue(backwardSpeed);
    }
    private void rcLeft() {
        directionServo.setValue(2000);
    }
    private void rcRight() 
    {
        directionServo.setValue(1000);
    }
    private void rcStraight()
    {
        directionServo.setValue(1500);
    }
    private void stop() {
        speedServo.setValue(0);
    }
    
    /* Right command for autonomous control */
    private void right(long time) {
	directionServo.setValue(0);
        directionServo.setValue(1000);
        pause(time);
        directionServo.setValue(1500);
    }
    
    /* Left command for autonomous control */
    private void left(long time) {
	directionServo.setValue(0);
        directionServo.setValue(2000);
        pause(time);
        directionServo.setValue(1500);
    }
    
    /* Straight command for autonomous control */
    private void straight (long time) {
        directionServo.setValue(1500);
        pause(time);
    }

    
    
    public void switchPressed(SwitchEvent sw) {
        
        if (sw.getSwitch() == sw2) { // for velocity change
	    System.out.println("SW2 Pressed");
            changeTurning(true);
        }
    }

    public void switchReleased(SwitchEvent sw) {
        // do nothing
    }

    protected void pauseApp() {
        // This will never be called by the Squawk VM
    }

    /**
     * Called if the MIDlet is terminated by the system. I.e. if startApp throws
     * any exception other than MIDletStateChangeException, if the isolate
     * running the MIDlet is killed with Isolate.exit(), or if VM.stopVM() is
     * called.
     *
     * It is not called if MIDlet.notifyDestroyed() was called.
     *
     * @param unconditional If true when this method is called, the MIDlet must
     * cleanup and release all resources. If false the MIDlet may throw
     * MIDletStateChangeException to indicate it does not want to be destroyed
     * at this time.
     */
    protected void destroyApp(boolean unconditional) throws MIDletStateChangeException {
        for (int i = 0; i < myLEDs.size(); i++) {
            myLEDs.getLED(i).setOff();
        }
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sunspotworld;

/**
 *
 * @author Raphy
 */
public class DistPair
{
    double leftDistance;
    double rightDistance;
    
    DistPair(double l, double r)
    {
        this.leftDistance = l;
        this.rightDistance = r;
    }
    
    public double getLeft()
    {
        return leftDistance;
    }
    
    public double getRight()
    {
        return rightDistance;
    }
}

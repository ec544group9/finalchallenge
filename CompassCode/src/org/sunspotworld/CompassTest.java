/*
 * CompassTest.java
 * Experimental Code for using the eDemo IIC interface
 * This uses the digital compass HMC6352 by Honeywell Inc.
 * Connect the GND to GND on the eDemo, Vcc to +3V, SDA to D2 via 10k ohms res,
 * and SCL to D3 via 10k ohms res.
 * 
 * Created on Jul 30, 2012 10:59:32 AM;
 */


package org.sunspotworld;

//import com.sun.spot.sensorboard.EDemoBoard;
import com.sun.spot.peripheral.II2C;
import com.sun.spot.resources.Resources;
import com.sun.spot.util.Utils;
import java.io.IOException;

import com.sun.spot.peripheral.radio.RadioFactory;
//import com.sun.spot.resources.transducers.ISwitch;
//import com.sun.spot.resources.transducers.ITriColorLED;
import com.sun.spot.resources.transducers.ITriColorLEDArray;
import com.sun.spot.resources.transducers.LEDColor;
import com.sun.spot.service.BootloaderListenerService;
import com.sun.spot.util.IEEEAddress;
import com.sun.spot.util.Utils;

import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;

/**
 * The startApp method of this class is called by the VM to start the
 * application.
 * 
 * The manifest specifies this class as MIDlet-1, which means it will
 * be selected for execution.
 * 
 * @author Yuting Zhang <ytzhang@bu.edu>
 *
 */


public class CompassTest extends MIDlet {
    
    private final static byte SLA_HMC6352 = (byte) 0x42;
    private static final byte SLA_HMC6352_READ_COMMAND = 'A';
    private static final byte SLA_HMC6352_WRITE_RAM = 'G';
        
    private boolean dev = true;
    

    private II2C i2c = (II2C) Resources.lookup(II2C.class, "location=eDemoboard");
    private ITriColorLEDArray leds = (ITriColorLEDArray) Resources.lookup(ITriColorLEDArray.class);
    
  private int readEEPROM(int n){
      byte[] snd = new byte[2];
      snd[0] = (byte) 0x72;
      snd[1] = (byte) (n & 0xFF);
      byte[] rcv = new byte[1];
      try{
      i2c.write(SLA_HMC6352, snd, 0, 2);
      
      i2c.read(SLA_HMC6352, rcv, 0, 1);
      }catch (IOException e){
            System.out.println("getEEPROM"+e);
        }
      int opmode = (int) rcv[0];
      return opmode;
      
      
    
    }
    
    private int readRAM(){
      byte[] snd = new byte[2];
      snd[0] = (byte) 0x67;
      snd[1] = (byte) 0x74;
      byte[] rcv = new byte[1];
      try{
      i2c.write(SLA_HMC6352, snd, 0, 2);
      i2c.read(SLA_HMC6352, rcv, 0, 1);
      }catch (IOException e){
            System.out.println("getRAM"+e);
        }
      int opmode = (int) rcv[0];
      return opmode;
    }
    
    private double getHeading(){
        byte[] snd = new byte[1];
        snd[0] = (byte) 0x41;
        byte[] rcv = new byte[2];
	double headingValue = 0.0;
        try{
            i2c.write(SLA_HMC6352,SLA_HMC6352_READ_COMMAND, 1, snd,0,1);
	    i2c.read(SLA_HMC6352, rcv, 0, 2);
	    System.out.println("The reading is " + Integer.toBinaryString(rcv[0]) + " " + Integer.toBinaryString(rcv[1]));
                    
	    headingValue = (rcv[0] << 8) + rcv[1];
//            headingValue = ((int) headingValue / 10) + ((headingValue % 10) / 10);
//	    headingValue /= 10.0;
        }catch(IOException e){
            System.out.println("getMegnetics"+e);
        }
//       int mag = (int) ((rcv[0]&0xFF)|(rcv[1]&0x1));
       //int mag = (int) rcv[0];
//        return ((double)mag)/10.0;
	return headingValue;
    }
    
    private void run() throws IOException{
        System.out.println("Start eDemo IIC communication");
       
 
        for (int j=0;j<=8;j++){
        System.out.println("Reading in EEPROM "+readEEPROM(j));
        }
        System.out.println("Operation mode in RAM " + readRAM());
        
        while(true){
            System.out.println("Magnetics" + getHeading() + "degree");
            Utils.sleep(100);
        }
    
    }
    protected void startApp() throws MIDletStateChangeException {
        System.out.println("Hello, world");
        BootloaderListenerService.getInstance().start();   // monitor the USB (if connected) and recognize commands from host

        long ourAddr = RadioFactory.getRadioPolicyManager().getIEEEAddress();
        System.out.println("Our radio address = " + IEEEAddress.toDottedHex(ourAddr));

        for (int i = 0; i < leds.size(); i++) {
                        leds.getLED(i).setColor(LEDColor.GREEN);
                        leds.getLED(i).setOn();
                    }
        Utils.sleep(500);
        for (int i = 0; i < leds.size(); i++) { 
                        leds.getLED(i).setOff(); 
                    }
        try{
            i2c.open();
            dev = i2c.probe(SLA_HMC6352,55);

            System.out.println("HMC6352 present? "+dev);
            run();
        } catch(IOException ex){
            ex.printStackTrace();
        }finally{
        try{
            i2c.close();
        }catch(IOException ex){}
        }
        
 /*       if(WRITE_ME){
        }*/ 
        

        
        
        
    }

    protected void pauseApp() {
        // This is not currently called by the Squawk VM
    }

    /**
     * Called if the MIDlet is terminated by the system.
     * It is not called if MIDlet.notifyDestroyed() was called.
     *
     * @param unconditional If true the MIDlet must cleanup and release all resources.
     */
    protected void destroyApp(boolean unconditional) throws MIDletStateChangeException {
        for (int i = 0; i < leds.size(); i++) {
            leds.getLED(i).setOff();
        }
    }
}

/*
 * SunSpotApplication.java
 *
 * Created on Nov 15, 2012 1:44:50 AM;
 */

package org.sunspotworld;

import com.sun.spot.peripheral.Spot;
import com.sun.spot.peripheral.TimeoutException;
import com.sun.spot.peripheral.radio.IProprietaryRadio;
import com.sun.spot.peripheral.radio.IRadioPolicyManager;
import com.sun.spot.peripheral.radio.RadioFactory;
//import com.sun.spot.peripheral.radio.routing.RoutingPolicyManager;
//import com.sun.spot.peripheral.radio.mhrp.aodv.AODVManager;
//import com.sun.spot.peripheral.radio.shrp.SingleHopManager;
import com.sun.spot.util.IEEEAddress;

import com.sun.spot.resources.Resources;
import com.sun.spot.resources.transducers.ILed;
import com.sun.spot.resources.transducers.ISwitch;
import com.sun.spot.resources.transducers.ILightSensor;
import com.sun.spot.resources.transducers.ITriColorLED;
import com.sun.spot.resources.transducers.LEDColor;
import com.sun.spot.resources.transducers.ITriColorLEDArray;
import com.sun.spot.resources.transducers.IAccelerometer3D;
import com.sun.spot.io.j2me.radiogram.Radiogram;
import com.sun.spot.io.j2me.radiogram.RadiogramConnection;

import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.Datagram;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;

/**
 * Routines to turn multiple SPOTs broadcasting their information and discover
 * peers in the same PAN
 *
 * The SPOT uses the LEDs to display its status as follows:
 * LED 0:
 * Red = missed an expected packet
 * Green = received a packet
 *
 * LED 1-5:
 * Leader displayed count of connected spots in green
 * 
 * LED 6:
 * display TiltChange flag when sw1 is pressed
 * Blue = false
 * Green = true
 * 
 * LED 7:
 * Red = right tilt 
 * Blue = left tilt
 *
 * Press right witch to change neighbors' tilt state:
 * by sending out tilt change flag in the datagram
 * SW1 = change neighbors' tilt
 * 
 * Switch 2 right now is used to adjust transmitting power
 *
 * Note: Each group need to use their own channels to avoid interference from others
 * channel 26 is default
 * Group 1: 11-13
 * Group 2: 14-16
 * Group 4: 20-22
 * Group 5: 23-25
 * Group 6: 17-19
 * 
 * 
 * @author Yuting Zhang
 * date: Nov 28,2012
 * Note: this work is base on Radio Strength demo from Ron Goldman
 */
public class BeaconDiscovery extends MIDlet {

    private static final String VERSION = "1.0";
    // CHANNEL_NUMBER  default as 26, each group set their own correspondingly
    private static final int CHANNEL_NUMBER = IProprietaryRadio.DEFAULT_CHANNEL; 
    private static final short PAN_ID               = IRadioPolicyManager.DEFAULT_PAN_ID;
    private static final String BROADCAST_PORT      = "42";
    private static final int PACKETS_PER_SECOND     = 1;
    private static final int PACKET_INTERVAL        = 1000 / PACKETS_PER_SECOND;
    
    private int channel = 18;
    private int power = 32;                             // Start with max transmit power
    
//    private ISwitch sw1 = (ISwitch)Resources.lookup(ISwitch.class, "SW1");
//    private ISwitch sw2 = (ISwitch)Resources.lookup(ISwitch.class, "SW2");
    private ITriColorLEDArray leds = (ITriColorLEDArray)Resources.lookup(ITriColorLEDArray.class);
    private ITriColorLED statusLED = leds.getLED(0);//connection indicator
    private ITriColorLED xmitLED = leds.getLED(7);//sending request indicator
    private ITriColorLED recvLED = leds.getLED(6);//receving distance indicator
    private ITriColorLED passLED = leds.getLED(2);
//    private IAccelerometer3D accel = (IAccelerometer3D)Resources.lookup(IAccelerometer3D.class);
//    private ILightSensor light = (ILightSensor)Resources.lookup(ILightSensor.class);

    private LEDColor red   = new LEDColor(100,0,0);
    private LEDColor green = new LEDColor(0,100,0);
    private LEDColor blue  = new LEDColor(0,0,100);
    
    private boolean xmitDo = true;
    private boolean recvDo = true;
    
    private long myAddr;
    private long TimeStamp;
   
    private String[] becon={"0014.4F01.0000.7F48","0014.4F01.0000.45A9"};
    private int order=0;

     /**
     * Loop to continually broadcast message.
     * message format
     * (long)myAddr,(long)follower,(long)leader,(long)TimeStamp,(int)tilt,
     * (boolean)tiltchang,(int)power,(int)count
     */
        private void xmitLoop () {
 //       ILed led = Spot.getInstance().getGreenLed();
        RadiogramConnection txConn = null;
        xmitDo = true;
        while (xmitDo) {
            try {
                txConn = (RadiogramConnection)Connector.open("radiogram://broadcast:" + BROADCAST_PORT);
                txConn.setMaxBroadcastHops(1);      // don't want packets being rebroadcasted
                Datagram xdg = txConn.newDatagram(txConn.getMaximumLength());

                    xmitLED.setColor(green);
                    xmitLED.setOn();
                    TimeStamp = System.currentTimeMillis();

                    xdg.reset();
                    xdg.writeLong(myAddr); // own MAC address
                    xdg.writeInt(order);
                    txConn.send(xdg);
//                    led.setOff();
                    pause(500);
                    xmitLED.setOff();   
                    System.out.println("sending to"+order+"becon");
                    long delay = (TimeStamp+ PACKET_INTERVAL- System.currentTimeMillis());
                    if (delay > 0) {
                        pause(delay);
                    }
            } catch (IOException ex) {
                // ignore
            } finally {
                if (txConn != null) {
                    try {
                        txConn.close();
                    } catch (IOException ex) { }
                }
            }
        }
    }
    
    /**
     * Loop to receive packets and discover peers information
     * (long)srcAddr,(long)srcLeader,(long)srcFollow,(long)srcTime,(int)srcSTEER,(int)srcSPEED
     * 
     * [TO DO]
     * sort out leader-follower by their MAC address order 
     * very most leader needs to know himself as the leader, then launch movement
     * very most follower needs to know himself as the last 
     */
    private void recvLoop () {
        RadiogramConnection rcvConn = null;
        recvDo = true;

        while (recvDo) {
            try {
                rcvConn = (RadiogramConnection)Connector.open("radiogram://:" + BROADCAST_PORT);
                rcvConn.setTimeout(PACKET_INTERVAL * 2);
                Radiogram rdg = (Radiogram)rcvConn.newDatagram(rcvConn.getMaximumLength());

                    try {
                        rdg.reset();
                        rcvConn.receive(rdg);           // listen for a packet

                        statusLED.setColor(green);
                        statusLED.setOn();
                        
                            recvLED.setColor(blue);
                            recvLED.setOn();
                            long bcAddr = rdg.readLong();
                            long bcTime = rdg.readLong();
                            double dist = rdg.readDouble();
                            int pow = rdg.readInt();
                            
                            String srcID = IEEEAddress.toDottedHex(bcAddr);
//                            System.out.println(srcID+','+bcTime+','+dist);
                            
                            long beconadd = IEEEAddress.toLong(becon[order]);
                            if(bcAddr==beconadd){
                               if(dist<= 80){
                                System.out.println("pass through"+order+"becon"); 
                                passLED.setColor(red);
                                passLED.setOn();
                             
                                order = (order + 1) >= becon.length ? 0 : order + 1;            
                               }
                                
                            }
                            
                            pause(500);
                            recvLED.setOff();
                            passLED.setOff();
                            
//                            long delay = (TimeStamp+ PACKET_INTERVAL- System.currentTimeMillis());
//                    if (delay > 0) {
//                        pause(delay);
//                    }
                            
                    } catch (TimeoutException tex) {        // timeout - display no packet received
                        statusLED.setColor(red);
                        statusLED.setOn();
                        System.out.println("time out!");
                    }               
            } catch (IOException ex) {
                // ignore
            } finally {
                if (rcvConn != null) {
                    try {
                        rcvConn.close();
                    } catch (IOException ex) { }
                }
            }
        }
    }
    
    
    /**
     * Pause for a specified time.
     *
     * @param time the number of milliseconds to pause
     */
    private void pause (long time) {
        try {
            Thread.currentThread().sleep(time);
        } catch (InterruptedException ex) { /* ignore */ }
    }
    

    /**
     * Initialize any needed variables.
     */
    private void initialize() { 
        myAddr = RadioFactory.getRadioPolicyManager().getIEEEAddress();
        statusLED.setColor(red);     // Red = not active
        statusLED.setOn();
        
        IRadioPolicyManager rpm = Spot.getInstance().getRadioPolicyManager();
        rpm.setChannelNumber(channel);
        rpm.setPanId(PAN_ID);
        rpm.setOutputPower(power - 32);
    //    AODVManager rp = Spot.getInstance().
        pause(1000);
        statusLED.setOff();
    }
    

    /**
     * Main application run loop.
     */
    private void run() {
        System.out.println("Radio Signal Strength Test (version " + VERSION + ")");
        System.out.println("Packet interval = " + PACKET_INTERVAL + " msec");
        
        new Thread() {
            public void run () {
                xmitLoop();
            }
        }.start();                      // spawn a thread to transmit packets
        new Thread() {
            public void run () {
                recvLoop();
            }
        }.start();                      // spawn a thread to receive packets
      //  respondToSwitches();            // this thread will handle User input via switches
    }


    
    /**
     * MIDlet call to start our application.
     */
    protected void startApp() throws MIDletStateChangeException {
	// Listen for downloads/commands over USB connection
	new com.sun.spot.service.BootloaderListenerService().getInstance().start();
        initialize();
        run();
    }

    /**
     * This will never be called by the Squawk VM.
     */
    protected void pauseApp() {
        // This will never be called by the Squawk VM
    }

    /**
     * Called if the MIDlet is terminated by the system.
     * @param unconditional If true the MIDlet must cleanup and release all resources.
     */
    protected void destroyApp(boolean unconditional) throws MIDletStateChangeException {
    }

}
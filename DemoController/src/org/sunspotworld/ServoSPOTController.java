/*
 * ServoSPOTController.java
 *
 * Created on Jul 19, 2012 9:46:39 PM;
 */

package org.sunspotworld;

import com.sun.spot.io.j2me.radiogram.RadiogramConnection;
import com.sun.spot.peripheral.Spot;
import com.sun.spot.peripheral.radio.IRadioPolicyManager;
import com.sun.spot.sensorboard.EDemoBoard;
import com.sun.spot.resources.transducers.IAccelerometer3D;

import com.sun.spot.resources.Resources;
import com.sun.spot.resources.transducers.ISwitch;
import com.sun.spot.resources.transducers.ISwitchListener;
import com.sun.spot.resources.transducers.ITriColorLED;
import com.sun.spot.resources.transducers.ITriColorLEDArray;
import com.sun.spot.resources.transducers.LEDColor;
import com.sun.spot.resources.transducers.SwitchEvent;
import com.sun.spot.service.BootloaderListenerService;
import com.sun.spot.util.Utils;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.Datagram;

import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;
import org.sunspotworld.lib.LedUtils;

/**
 * This class is used to control a servo car remotely. This sends values
 * measured by demoboard accelerometer to the servo car.
 * 
 * You must specify buddyAddress, that is the SPOT address on the car to
 * communicate each other.
 * 
 * @author Tsuyoshi Miyake <Tsuyoshi.Miyake@Sun.COM>
 * @author Yuting Zhang<ytzhang@bu.edu>
 */
public class ServoSPOTController extends MIDlet implements ISwitchListener {

    private EDemoBoard eDemo = EDemoBoard.getInstance();
    private IAccelerometer3D accel = (IAccelerometer3D)Resources.lookup(IAccelerometer3D.class);
    private ITriColorLEDArray myLEDs = (ITriColorLEDArray) Resources.lookup(ITriColorLEDArray.class);
    private static final String BROADCAST_PORT      = "19";
    private static boolean remoteControlled=false;
    private ISwitch sw1 = eDemo.getSwitches()[EDemoBoard.SW1];
    private ISwitch sw2 = eDemo.getSwitches()[EDemoBoard.SW2];
    private int channel = 18;
    private static final int CHANNEL_NUMBER = 19;
    private static final short PAN_ID               = 9;
    private boolean doTurn = false;
    private ITriColorLED statusLED = myLEDs.getLED(0);
    private ITriColorLED turnLED = myLEDs.getLED(7);
    
    private int xtilt;
    private int ytilt;
    
    protected void startApp() throws MIDletStateChangeException {
        System.out.println("Hello, world");
        BootloaderListenerService.getInstance().start();  
	initialize();
        
        for (int i = 0; i < myLEDs.size(); i++) {
                        myLEDs.getLED(i).setColor(LEDColor.GREEN);
                        myLEDs.getLED(i).setOn();
                    }
        Utils.sleep(500);
        for (int i = 0; i < myLEDs.size(); i++) {
                        myLEDs.getLED(i).setOff(); 
                    }
        
//        BlinkenLights blinker = new BlinkenLights();
//        blinker.startPsilon();
	statusLED.setColor(LEDColor.RED);
	turnLED.setColor(LEDColor.WHITE);
	turnLED.setOff();
	statusLED.setOn();
	sw1.addISwitchListener(this);
	sw2.addISwitchListener(this);
//        blinker.setColor(LEDColor.BLUE);
	new Thread() {
            public void run () {
                xmitLoop();
            }
        }.start();
	
	new Thread() {
            public void run () {
                accelLoop();
            }
        }.start();
	
    }
    
    public void accelLoop()
    {
	while (true)
	{
	    try
	    {
		xtilt=(int)(accel.getTiltX() * 100);
		ytilt=(int)(accel.getTiltY() * 100);
	    }
	    catch (IOException e)
	    {
		System.out.println("Error reading accelerometer");
	    }
	}
    }
    
    public void switchPressed(SwitchEvent sw) {
        
        if (sw.getSwitch() == sw1) { // for velocity change
	    remoteControlled = true;
	    statusLED.setColor(LEDColor.GREEN);
	    statusLED.setOn();
        }
	else if (sw.getSwitch() == sw2)
	{
	    remoteControlled = false;
	    statusLED.setColor(LEDColor.RED);
	    statusLED.setOn();
	}
    }

    public void switchReleased(SwitchEvent sw) {
        // do nothing
    }
    
       private void initialize() { 
        
        IRadioPolicyManager rpm = Spot.getInstance().getRadioPolicyManager();
        rpm.setChannelNumber(CHANNEL_NUMBER);
        rpm.setPanId(PAN_ID);
//        rpm.setOutputPower(power - 32);
    //    AODVManager rp = Spot.getInstance().
        Utils.sleep(1000);
//        statusLED.setOff();
    } 
       
   /**
     * Pause for a specified time.
     *
     * @param time the number of milliseconds to pause
     */
    private void pause (long time) {
        try {
            Thread.currentThread().sleep(time);
        } catch (InterruptedException ex) { /* ignore */ }
    }
    
    
    private void xmitLoop () {
 //       ILed led = Spot.getInstance().getGreenLed();
        RadiogramConnection txConn = null;
       
        while (true) {
            try {
                txConn = (RadiogramConnection)Connector.open("radiogram://broadcast:" + BROADCAST_PORT);
                txConn.setMaxBroadcastHops(1);      // don't want packets being rebroadcasted
                Datagram xdg = txConn.newDatagram(txConn.getMaximumLength());
		
		System.out.println("Sending message " + 1 + " " + remoteControlled + " " + xtilt + " " + ytilt);
		xdg.reset();
                xdg.writeInt(1);
		    if (remoteControlled)
			xdg.writeInt(1);
		    else
			xdg.writeInt(0);
		
		/* Write the xtilt global variables */
		xdg.writeInt(xtilt);
		xdg.writeInt(ytilt);
		txConn.send(xdg);

		if (!remoteControlled)
		{
		    pause(500);
		}
		
		
		
            } catch (IOException ex) {
                System.out.println("can not find car");
            } finally {
                if (txConn != null) {
                    try {
                        txConn.close();
                    } catch (IOException ex) { }
                }
            }
        }
    }

    protected void pauseApp() {
        // This is not currently called by the Squawk VM
    }

    /**
     * Called if the MIDlet is terminated by the system.
     * It is not called if MIDlet.notifyDestroyed() was called.
     *
     * @param unconditional If true the MIDlet must cleanup and release all resources.
     */
    protected void destroyApp(boolean unconditional) throws MIDletStateChangeException {
        LedUtils.setOffAll();
    }
}
